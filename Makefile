# see: http://www.ocaml.info/home/ocaml_sources.html#toc16

THREADS = yes
# put here the names of your source files (in the right order)
SOURCES=gphoto2.idl eArray.ml constant.ml camera.mli camera.ml database.ml model.mli model.ml ihm.ml control.ml

# the name of the resulting executable
RESULT=libgphoto2
#OCAMLFLAGS=-i

# generate type information (.annot files)
ANNOTATE = yes
NOIDLHEADER = yes

INCDIRS=+lablgtk2 +sqlite3
CC=gcc
CLIBS=gphoto2 gphoto2_port
LFLAGS=-L/usr/local/lib
CPPFLAGS=-I/usr/local/include
# make target (see manual) : byte-code, debug-code, native-code, ...
all: byte-code-library bookscanning custom-toplevel

custom-toplevel: libgphoto2.cma
	ocamlmktop -thread -o ocaml-gphoto2 -I +lablgtk2 -I +sqlite3 unix.cma lablgtk.cma lablgnomecanvas.cma threads.cma gtkThread.cmo sqlite3.cma gtkInit.cmo libgphoto2.cma -cclib '-L. -L/usr/local/lib'
include OCamlMakefile

bookscanning: libgphoto2.cma main.cmo
	ocamlc -thread -o bookscanning -I +lablgtk2 -I +sqlite3 unix.cma lablgtk.cma lablgnomecanvas.cma threads.cma gtkThread.cmo sqlite3.cma gtkInit.cmo libgphoto2.cma main.cmo -cclib '-L. -L/usr/local/lib'

main.cmo: main.ml libgphoto2.cma
control.cmi: control.ml gphoto2.cmi
