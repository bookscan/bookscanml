type side = LEFT | RIGHT

type move_command = START | PREV | OK | NEXT | END
type camera_selection = S_LEFT | S_RIGHT | S_BOTH


type page = {
	camera_path : Gphoto2.cameraFilePath;
	thumbname : string;
	mutable loaded : string option
} 

let _BORDER = 20
let _WIN_WIDTH = 260 and _WIN_HEIGHT = 340
let _ARROW_WIDTH = 400 and _ARROW_HEIGHT = 30

let _START_ICON = "start.xpm" and _END_ICON = "end.xpm" and
    _PREV_ICON = "previous.xpm" and _NEXT_ICON = "next.xpm" and 
    _PHOTO_ICON = "photo.xpm" and _PHOTO_BUSY_ICON = "photo_busy.xpm"
let _LIB_FOLDER = "bookscanning"

let _CAMERAS_KEY = "CAMERAS"
let _FOCUS_LEFT_KEY = "FOCUS_LEFT"
let _FOCUS_RIGHT_KEY = "FOCUS_RIGHT"

let debug = ref false