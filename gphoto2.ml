(* File generated from gphoto2.idl *)

type camera_ptr
and gPContext_ptr
and cameraFile_ptr
and gPPortInfoList_ptr
and cameraAbilitiesList_ptr
and cameraList_ptr
and cameraWidget_ptr
and gPPortInfo
and enum_1 =
  | GP_CAPTURE_IMAGE
  | GP_CAPTURE_MOVIE
  | GP_CAPTURE_SOUND
and cameraCaptureType = enum_1
and _GPPortType =
  | GP_PORT_NONE
  | GP_PORT_SERIAL
  | GP_PORT_USB
  | GP_PORT_DISK
  | GP_PORT_PTPIP
and gPPortType = _GPPortType list
and enum_3 =
  | GP_DRIVER_STATUS_PRODUCTION
  | GP_DRIVER_STATUS_TESTING
  | GP_DRIVER_STATUS_EXPERIMENTAL
  | GP_DRIVER_STATUS_DEPRECATED
and cameraDriverStatus = enum_3
and enum_4 =
  | GP_DEVICE_STILL_CAMERA
  | GP_DEVICE_AUDIO_PLAYER
and gphotoDeviceType = enum_4
and _CameraOperation =
  | GP_OPERATION_NONE
  | GP_OPERATION_CAPTURE_IMAGE
  | GP_OPERATION_CAPTURE_VIDEO
  | GP_OPERATION_CAPTURE_AUDIO
  | GP_OPERATION_CAPTURE_PREVIEW
  | GP_OPERATION_CONFIG
and cameraOperation = _CameraOperation list
and _CameraFileOperation =
  | GP_FILE_OPERATION_NONE
  | GP_FILE_OPERATION_DELETE
  | GP_FILE_OPERATION_PREVIEW
  | GP_FILE_OPERATION_RAW
  | GP_FILE_OPERATION_AUDIO
  | GP_FILE_OPERATION_EXIF
and cameraFileOperation = _CameraFileOperation list
and _CameraFolderOperation =
  | GP_FOLDER_OPERATION_NONE
  | GP_FOLDER_OPERATION_DELETE_ALL
  | GP_FOLDER_OPERATION_PUT_FILE
  | GP_FOLDER_OPERATION_MAKE_DIR
  | GP_FOLDER_OPERATION_REMOVE_DIR
and cameraFolderOperation = _CameraFolderOperation list
and _CameraStorageInfoFields =
  | GP_STORAGEINFO_BASE
  | GP_STORAGEINFO_LABEL
  | GP_STORAGEINFO_DESCRIPTION
  | GP_STORAGEINFO_ACCESS
  | GP_STORAGEINFO_STORAGETYPE
  | GP_STORAGEINFO_FILESYSTEMTYPE
  | GP_STORAGEINFO_MAXCAPACITY
  | GP_STORAGEINFO_FREESPACEKBYTES
  | GP_STORAGEINFO_FREESPACEIMAGES
and cameraStorageInfoFields = _CameraStorageInfoFields list
and enum_9 =
  | GP_STORAGEINFO_ST_UNKNOWN
  | GP_STORAGEINFO_ST_FIXED_ROM
  | GP_STORAGEINFO_ST_REMOVABLE_ROM
  | GP_STORAGEINFO_ST_FIXED_RAM
  | GP_STORAGEINFO_ST_REMOVABLE_RAM
and cameraStorageType = enum_9
and enum_10 =
  | GP_STORAGEINFO_AC_READWRITE
  | GP_STORAGEINFO_AC_READONLY
  | GP_STORAGEINFO_AC_READONLY_WITH_DELETE
and cameraStorageAccessType = enum_10
and enum_11 =
  | GP_STORAGEINFO_FST_UNDEFINED
  | GP_STORAGEINFO_FST_GENERICFLAT
  | GP_STORAGEINFO_FST_GENERICHIERARCHICAL
  | GP_STORAGEINFO_FST_DCF
and cameraStorageFilesystemType = enum_11
and _CameraStorageInformation = {
  store_fields: cameraStorageInfoFields;
  store_basedir: string;
  store_label: string;
  store_description: string;
  store_type: cameraStorageType;
  store_fstype: cameraStorageFilesystemType;
  store_access: cameraStorageAccessType;
  store_capacity: int;
  store_free: int;
  store_freeimages: int;
}
and cameraStorageInformation = _CameraStorageInformation
and struct_13 = {
  model: string;
  status: cameraDriverStatus;
  port: gPPortType;
  speed: int array;
  operations: cameraOperation;
  file_operations: cameraFileOperation;
  folder_operations: cameraFolderOperation;
  usb_vendor: int;
  usb_product: int;
  usb_class: int;
  usb_subclass: int;
  usb_protocol: int;
  library: string;
  id: string;
  device_type: gphotoDeviceType;
  reserved2: int;
  reserved3: int;
  reserved4: int;
  reserved5: int;
  reserved6: int;
  reserved7: int;
  reserved8: int;
}
and cameraAbilities = struct_13
and enum_14 =
  | GP_FILE_TYPE_PREVIEW
  | GP_FILE_TYPE_NORMAL
  | GP_FILE_TYPE_RAW
  | GP_FILE_TYPE_AUDIO
  | GP_FILE_TYPE_EXIF
  | GP_FILE_TYPE_METADATA
and cameraFileType = enum_14
and enum_15 =
  | GP_WIDGET_WINDOW
  | GP_WIDGET_SECTION
  | GP_WIDGET_TEXT
  | GP_WIDGET_RANGE
  | GP_WIDGET_TOGGLE
  | GP_WIDGET_RADIO
  | GP_WIDGET_MENU
  | GP_WIDGET_BUTTON
  | GP_WIDGET_DATE
and cameraWidgetType = enum_15
and fdescr = Unix.file_descr
and struct_16 = {
  name: string;
  folder: string;
}
and cameraFilePath = struct_16

external gp_context_new : unit -> gPContext_ptr
	= "camlidl_gphoto2_gp_context_new"

external debug_init : unit -> unit
	= "camlidl_gphoto2_debug_init"

external gp_port_info_list_new : unit -> int * gPPortInfoList_ptr
	= "camlidl_gphoto2_gp_port_info_list_new"

external gp_port_info_list_free : gPPortInfoList_ptr -> int
	= "camlidl_gphoto2_gp_port_info_list_free"

external gp_port_info_list_load : gPPortInfoList_ptr -> int
	= "camlidl_gphoto2_gp_port_info_list_load"

external gp_port_info_list_count : gPPortInfoList_ptr -> int
	= "camlidl_gphoto2_gp_port_info_list_count"

external gp_port_info_list_get_info : gPPortInfoList_ptr -> int -> int * gPPortInfo
	= "camlidl_gphoto2_gp_port_info_list_get_info"

external gp_port_info_list_lookup_path : gPPortInfoList_ptr -> string -> int
	= "camlidl_gphoto2_gp_port_info_list_lookup_path"

external gp_port_info_list_lookup_name : gPPortInfoList_ptr -> string -> int
	= "camlidl_gphoto2_gp_port_info_list_lookup_name"

external gp_port_info_new : unit -> int * gPPortInfo
	= "camlidl_gphoto2_gp_port_info_new"

external gp_port_info_get_name : gPPortInfo -> int * string
	= "camlidl_gphoto2_gp_port_info_get_name"

external gp_port_info_set_name : gPPortInfo -> string -> int
	= "camlidl_gphoto2_gp_port_info_set_name"

external gp_port_info_get_path : gPPortInfo -> int * string
	= "camlidl_gphoto2_gp_port_info_get_path"

external gp_port_info_set_path : gPPortInfo -> string -> int
	= "camlidl_gphoto2_gp_port_info_set_path"

external gp_port_info_get_type : gPPortInfo -> gPPortType option -> int
	= "camlidl_gphoto2_gp_port_info_get_type"

external gp_port_info_set_type : gPPortInfo -> gPPortType -> int
	= "camlidl_gphoto2_gp_port_info_set_type"

external gp_file_new : unit -> int * cameraFile_ptr
	= "camlidl_gphoto2_gp_file_new"

external gp_file_new_from_fd : fdescr -> int * cameraFile_ptr
	= "camlidl_gphoto2_gp_file_new_from_fd"

external gp_file_free : cameraFile_ptr -> int
	= "camlidl_gphoto2_gp_file_free"

external gp_file_open : cameraFile_ptr -> string -> int
	= "camlidl_gphoto2_gp_file_open"

external gp_camera_file_delete : camera_ptr -> string -> string -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_file_delete"

external gp_camera_folder_list_files : camera_ptr -> string -> cameraList_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_folder_list_files"

external gp_camera_folder_list_folders : camera_ptr -> string -> cameraList_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_folder_list_folders"

external gp_camera_folder_delete_all : camera_ptr -> string -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_folder_delete_all"

external gp_camera_folder_put_file : camera_ptr -> string -> string -> cameraFileType -> cameraFile_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_folder_put_file_bytecode" "camlidl_gphoto2_gp_camera_folder_put_file"

external gp_camera_folder_make_dir : camera_ptr -> string -> string -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_folder_make_dir"

external gp_camera_folder_remove_dir : camera_ptr -> string -> string -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_folder_remove_dir"

external gp_abilities_list_new : unit -> int * cameraAbilitiesList_ptr
	= "camlidl_gphoto2_gp_abilities_list_new"

external gp_abilities_list_free : cameraAbilitiesList_ptr -> int
	= "camlidl_gphoto2_gp_abilities_list_free"

external gp_abilities_list_load : cameraAbilitiesList_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_abilities_list_load"

external gp_abilities_list_reset : cameraAbilitiesList_ptr -> int
	= "camlidl_gphoto2_gp_abilities_list_reset"

external gp_abilities_list_detect : cameraAbilitiesList_ptr -> gPPortInfoList_ptr -> cameraList_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_abilities_list_detect"

external gp_abilities_list_append : cameraAbilitiesList_ptr -> cameraAbilities -> int
	= "camlidl_gphoto2_gp_abilities_list_append"

external gp_abilities_list_count : cameraAbilitiesList_ptr -> int
	= "camlidl_gphoto2_gp_abilities_list_count"

external gp_abilities_list_lookup_model : cameraAbilitiesList_ptr -> string -> int
	= "camlidl_gphoto2_gp_abilities_list_lookup_model"

external gp_abilities_list_get_abilities : cameraAbilitiesList_ptr -> int -> int * cameraAbilities
	= "camlidl_gphoto2_gp_abilities_list_get_abilities"

external gp_list_new : unit -> int * cameraList_ptr
	= "camlidl_gphoto2_gp_list_new"

external gp_list_ref : cameraList_ptr -> int
	= "camlidl_gphoto2_gp_list_ref"

external gp_list_unref : cameraList_ptr -> int
	= "camlidl_gphoto2_gp_list_unref"

external gp_list_free : cameraList_ptr -> int
	= "camlidl_gphoto2_gp_list_free"

external gp_list_count : cameraList_ptr -> int
	= "camlidl_gphoto2_gp_list_count"

external gp_list_append : cameraList_ptr -> string -> string -> int
	= "camlidl_gphoto2_gp_list_append"

external gp_list_reset : cameraList_ptr -> int
	= "camlidl_gphoto2_gp_list_reset"

external gp_list_sort : cameraList_ptr -> int
	= "camlidl_gphoto2_gp_list_sort"

external gp_list_find_by_name : cameraList_ptr -> string -> int * int
	= "camlidl_gphoto2_gp_list_find_by_name"

external gp_list_get_name : cameraList_ptr -> int -> int * string
	= "camlidl_gphoto2_gp_list_get_name"

external gp_list_get_value : cameraList_ptr -> int -> int * string
	= "camlidl_gphoto2_gp_list_get_value"

external gp_list_set_name : cameraList_ptr -> int -> string -> int
	= "camlidl_gphoto2_gp_list_set_name"

external gp_list_set_value : cameraList_ptr -> int -> string -> int
	= "camlidl_gphoto2_gp_list_set_value"

external gp_list_populate : cameraList_ptr -> string -> int -> int
	= "camlidl_gphoto2_gp_list_populate"

external gp_widget_new : cameraWidgetType -> char option -> int * cameraWidget_ptr
	= "camlidl_gphoto2_gp_widget_new"

external gp_widget_append : cameraWidget_ptr -> cameraWidget_ptr -> int
	= "camlidl_gphoto2_gp_widget_append"

external gp_widget_prepend : cameraWidget_ptr -> cameraWidget_ptr -> int
	= "camlidl_gphoto2_gp_widget_prepend"

external gp_widget_count_children : cameraWidget_ptr -> int
	= "camlidl_gphoto2_gp_widget_count_children"

external gp_widget_get_child : cameraWidget_ptr -> int -> int * cameraWidget_ptr
	= "camlidl_gphoto2_gp_widget_get_child"

external gp_widget_get_child_by_label : cameraWidget_ptr -> string -> int * cameraWidget_ptr
	= "camlidl_gphoto2_gp_widget_get_child_by_label"

external gp_widget_get_child_by_id : cameraWidget_ptr -> int -> int * cameraWidget_ptr
	= "camlidl_gphoto2_gp_widget_get_child_by_id"

external gp_widget_get_child_by_name : cameraWidget_ptr -> string -> cameraWidget_ptr option -> int
	= "camlidl_gphoto2_gp_widget_get_child_by_name"

external gp_widget_get_root : cameraWidget_ptr -> int * cameraWidget_ptr
	= "camlidl_gphoto2_gp_widget_get_root"

external gp_widget_get_parent : cameraWidget_ptr -> int * cameraWidget_ptr
	= "camlidl_gphoto2_gp_widget_get_parent"

external gp_widget_set_value_int : cameraWidget_ptr -> int -> int
	= "camlidl_gphoto2_gp_widget_set_value_int"

external gp_widget_set_value_float : cameraWidget_ptr -> float -> int
	= "camlidl_gphoto2_gp_widget_set_value_float"

external gp_widget_set_value_string : cameraWidget_ptr -> string -> int
	= "camlidl_gphoto2_gp_widget_set_value_string"

external gp_widget_get_value_int : cameraWidget_ptr -> int * int
	= "camlidl_gphoto2_gp_widget_get_value_int"

external gp_widget_get_value_float : cameraWidget_ptr -> int * float
	= "camlidl_gphoto2_gp_widget_get_value_float"

external gp_widget_get_value_string : cameraWidget_ptr -> int * string
	= "camlidl_gphoto2_gp_widget_get_value_string"

external gp_widget_set_name : cameraWidget_ptr -> string -> int
	= "camlidl_gphoto2_gp_widget_set_name"

external gp_widget_get_name : cameraWidget_ptr -> int * string
	= "camlidl_gphoto2_gp_widget_get_name"

external gp_widget_set_info : cameraWidget_ptr -> string -> int
	= "camlidl_gphoto2_gp_widget_set_info"

external gp_widget_get_info : cameraWidget_ptr -> int * string
	= "camlidl_gphoto2_gp_widget_get_info"

external gp_widget_get_id : cameraWidget_ptr -> int * int
	= "camlidl_gphoto2_gp_widget_get_id"

external gp_widget_get_type : cameraWidget_ptr -> int * cameraWidgetType
	= "camlidl_gphoto2_gp_widget_get_type"

external gp_widget_get_label : cameraWidget_ptr -> int * string
	= "camlidl_gphoto2_gp_widget_get_label"

external gp_widget_set_range : cameraWidget_ptr -> float -> float -> float -> int
	= "camlidl_gphoto2_gp_widget_set_range"

external gp_widget_get_range : cameraWidget_ptr -> int * float * float * float
	= "camlidl_gphoto2_gp_widget_get_range"

external gp_widget_add_choice : cameraWidget_ptr -> string -> int
	= "camlidl_gphoto2_gp_widget_add_choice"

external gp_widget_count_choices : cameraWidget_ptr -> int
	= "camlidl_gphoto2_gp_widget_count_choices"

external gp_widget_get_choice : cameraWidget_ptr -> int -> int * string
	= "camlidl_gphoto2_gp_widget_get_choice"

external gp_widget_changed : cameraWidget_ptr -> int
	= "camlidl_gphoto2_gp_widget_changed"

external gp_widget_set_changed : cameraWidget_ptr -> int -> int
	= "camlidl_gphoto2_gp_widget_set_changed"

external gp_widget_set_readonly : cameraWidget_ptr -> int -> int
	= "camlidl_gphoto2_gp_widget_set_readonly"

external gp_widget_get_readonly : cameraWidget_ptr -> int * int
	= "camlidl_gphoto2_gp_widget_get_readonly"

external gp_camera_new : unit -> int * camera_ptr
	= "camlidl_gphoto2_gp_camera_new"

external gp_camera_init : camera_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_init"

external gp_camera_exit : camera_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_exit"

external gp_camera_free : camera_ptr -> int
	= "camlidl_gphoto2_gp_camera_free"

external gp_camera_capture : camera_ptr -> cameraCaptureType -> gPContext_ptr -> int * cameraFilePath
	= "camlidl_gphoto2_gp_camera_capture"

external gp_camera_capture_preview : camera_ptr -> cameraFile_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_capture_preview"

external gp_camera_file_get : camera_ptr -> string -> string -> cameraFileType -> cameraFile_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_file_get_bytecode" "camlidl_gphoto2_gp_camera_file_get"

external gp_camera_get_abilities : camera_ptr -> int * cameraAbilities
	= "camlidl_gphoto2_gp_camera_get_abilities"

external gp_camera_set_abilities : camera_ptr -> cameraAbilities -> int
	= "camlidl_gphoto2_gp_camera_set_abilities"

external gp_camera_set_port_info : camera_ptr -> gPPortInfo -> int
	= "camlidl_gphoto2_gp_camera_set_port_info"

external gp_camera_get_port_info : camera_ptr -> int * gPPortInfo
	= "camlidl_gphoto2_gp_camera_get_port_info"

external gp_camera_get_config : camera_ptr -> gPContext_ptr -> int * cameraWidget_ptr
	= "camlidl_gphoto2_gp_camera_get_config"

external gp_camera_set_config : camera_ptr -> cameraWidget_ptr -> gPContext_ptr -> int
	= "camlidl_gphoto2_gp_camera_set_config"

external gp_camera_get_storeinfo : camera_ptr -> gPContext_ptr -> cameraStorageInformation array * int
	= "camlidl_gphoto2_gp_camera_get_storeinfo"

