 class model :
   string ->
   object
		 (** name of left camera *)
     method camera_left : string
		 (** name of right camera *)
     method camera_right : string
		 (** download all the images from the camera 
         @param progess_notifier a function taking the current pos, the final pos and a continuation
			    to notify progress on the 
			   @param final_kont : a function taking the number of errors as parameter. Can notify the
			    number of problems. *)
     method download_all : (int -> int -> (unit -> unit) -> unit) -> (int -> unit) -> unit  
     
		 (** set the left focus to the value indicated
			  @param f value of focus 
		    @return thumbname of focus image *)
		 method focus_left : float -> string option
		 (** get the left focus *)
     method get_focus_left : (float * float * float * float) option
		 (** set the right focus to the value indicated
			   @param f value of focus 
		     @return thumbname of focus image *)
		 method focus_right : float -> string option
		 (** get the right focus *)
     method get_focus_right : (float * float * float * float) option
     
		 (** get information on files associated to a page
      @param i the position of a page
      @return name of the thumb and optionally the name of a page *)
     method get : int -> string * string option
     (** delete at the current position *)
		 method delete : bool
		 (** insert at the current position *)
     method insert : unit
 	   (** save to database *)
		 method save : unit
		 (** swap cameras *)
     method swap_cameras : unit
		 (** take photos on both cameras *)
     method take_both : unit
		 (** take photo on left camera *)
     method take_photo_left : unit
		 (** take photo on right camera *)
     method take_photo_right : unit
		 (** go to next page *)
     method next : unit
		 (** go to previous page *)
     method previous : unit
		 (** go to end *)
     method to_end : unit
		 (** go to first page *)
     method to_start : unit
		 (** file info on left page
	    @return name of the thumb and optionally the name of a page *)
     method left : string * string option
		 (** file info on right page
      @return name of the thumb and optionally the name of a page *)
     method right : string * string option
		 (** wipe all pages *)
     method wipe_all : bool
		 (** current position *)
		 method position : int * int
   end
