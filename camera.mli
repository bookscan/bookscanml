class camera :
  Gphoto2.gPContext_ptr ->
  Gphoto2.cameraAbilitiesList_ptr ->
  Gphoto2.cameraList_ptr ->
  Gphoto2.gPPortInfoList_ptr ->
  int -> 
  object
     (* val mutable rotate : string -> unit *)
    method get_focus : float 
		method get_focus_range : float * float * float
    method load_page : (unit -> string) -> Constant.page -> unit
    method name : string
    method set_focus : float -> unit
    method set_rotate : (string -> unit) -> unit
    method take_photo : string -> Constant.page
    method take_thumb : string -> string
    method wipe_all : unit
		method digest : string
  end
	
val configure_cameras : unit -> camera option * camera option
