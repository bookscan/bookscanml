/* File generated from gphoto2.idl */

#include <stddef.h>
#include <string.h>
#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/fail.h>
#include <caml/callback.h>
#ifdef Custom_tag
#include <caml/custom.h>
#include <caml/bigarray.h>
#endif
#include <caml/camlidlruntime.h>


#include "gphoto2.h"

#undef interface
#include <gphoto2/gphoto2-camera.h>
#include <gphoto2/gphoto2.h>
#include <stdio.h>
#include <stdarg.h>
typedef struct _Camera * Camera_ptr;
typedef struct _GPContext * GPContext_ptr;
typedef struct _CameraFile * CameraFile_ptr;
typedef struct _GPPortInfoList * GPPortInfoList_ptr;
typedef GPPortType GPPortTypeSet;
typedef struct _CameraAbilitiesList * CameraAbilitiesList_ptr;
typedef struct _CameraList * CameraList_ptr;
typedef struct _CameraWidget * CameraWidget_ptr;
typedef int fdescr;
static void errordumper(GPLogLevel level, const char *domain, const char *str, void *data) { fprintf(stdout,"%s\n",str); }
static void debug_init() { gp_log_add_func(GP_LOG_ERROR, errordumper, NULL); }
int gp_widget_get_value_int (CameraWidget_ptr widget, int *v){ return gp_widget_get_value(widget,v); }
int gp_widget_get_value_float (CameraWidget_ptr widget, float *v){ return gp_widget_get_value(widget,v); }
int gp_widget_get_value_string (CameraWidget_ptr widget, char **v){ return gp_widget_get_value(widget,v); }
int gp_widget_set_value_int (CameraWidget_ptr widget, int v){ return gp_widget_set_value(widget, (void *) &v); }
int gp_widget_set_value_float (CameraWidget_ptr widget, float v){ return gp_widget_set_value(widget,(void *) &v); }
int gp_widget_set_value_string (CameraWidget_ptr widget, char *v){ return gp_widget_set_value(widget,(void *) v); }
void finalize_camera(Camera_ptr *ptr) { gp_camera_unref(*ptr); }
void finalize_context(GPContext_ptr *ptr) { gp_context_unref(*ptr); }
void finalize_camera_file(CameraFile_ptr *ptr) {  gp_file_unref(*ptr); }
void finalize_port_info_list(GPPortInfoList_ptr *ptr) { gp_port_info_list_free(*ptr); }
void finalize_camera_abilities(CameraAbilitiesList_ptr *ptr) { gp_abilities_list_free(*ptr); }
void finalize_camera_list(CameraList_ptr *ptr) { gp_list_unref(*ptr); }
void finalize_port_info(GPPortInfo *ptr) {  }
void finalize_camera_widget(CameraWidget_ptr *ptr) { const char *n; gp_widget_get_name(*ptr,&n); gp_widget_unref(*ptr); }
void camlidl_ml2c_gphoto2_Camera_ptr(value _v1, Camera_ptr * _c2, camlidl_ctx _ctx)
{
  *_c2 = *((Camera_ptr *) Data_custom_val(_v1));
}

static void camlidl_finalize_gphoto2_Camera_ptr(value v)
{
  finalize_camera((Camera_ptr *) Data_custom_val(v));
}
struct custom_operations camlidl_cops_gphoto2_Camera_ptr = {
  NULL,
  camlidl_finalize_gphoto2_Camera_ptr,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

value camlidl_c2ml_gphoto2_Camera_ptr(Camera_ptr * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = alloc_custom(&camlidl_cops_gphoto2_Camera_ptr, sizeof(Camera_ptr), 0, 1);
  *((Camera_ptr *) Data_custom_val(_v1)) = *_c2;
  return _v1;
}

void camlidl_ml2c_gphoto2_GPContext_ptr(value _v1, GPContext_ptr * _c2, camlidl_ctx _ctx)
{
  *_c2 = *((GPContext_ptr *) Data_custom_val(_v1));
}

static void camlidl_finalize_gphoto2_GPContext_ptr(value v)
{
  finalize_context((GPContext_ptr *) Data_custom_val(v));
}
struct custom_operations camlidl_cops_gphoto2_GPContext_ptr = {
  NULL,
  camlidl_finalize_gphoto2_GPContext_ptr,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

value camlidl_c2ml_gphoto2_GPContext_ptr(GPContext_ptr * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = alloc_custom(&camlidl_cops_gphoto2_GPContext_ptr, sizeof(GPContext_ptr), 0, 1);
  *((GPContext_ptr *) Data_custom_val(_v1)) = *_c2;
  return _v1;
}

void camlidl_ml2c_gphoto2_CameraFile_ptr(value _v1, CameraFile_ptr * _c2, camlidl_ctx _ctx)
{
  *_c2 = *((CameraFile_ptr *) Data_custom_val(_v1));
}

static void camlidl_finalize_gphoto2_CameraFile_ptr(value v)
{
  finalize_camera_file((CameraFile_ptr *) Data_custom_val(v));
}
struct custom_operations camlidl_cops_gphoto2_CameraFile_ptr = {
  NULL,
  camlidl_finalize_gphoto2_CameraFile_ptr,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

value camlidl_c2ml_gphoto2_CameraFile_ptr(CameraFile_ptr * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = alloc_custom(&camlidl_cops_gphoto2_CameraFile_ptr, sizeof(CameraFile_ptr), 0, 1);
  *((CameraFile_ptr *) Data_custom_val(_v1)) = *_c2;
  return _v1;
}

void camlidl_ml2c_gphoto2_GPPortInfoList_ptr(value _v1, GPPortInfoList_ptr * _c2, camlidl_ctx _ctx)
{
  *_c2 = *((GPPortInfoList_ptr *) Data_custom_val(_v1));
}

static void camlidl_finalize_gphoto2_GPPortInfoList_ptr(value v)
{
  finalize_port_info_list((GPPortInfoList_ptr *) Data_custom_val(v));
}
struct custom_operations camlidl_cops_gphoto2_GPPortInfoList_ptr = {
  NULL,
  camlidl_finalize_gphoto2_GPPortInfoList_ptr,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

value camlidl_c2ml_gphoto2_GPPortInfoList_ptr(GPPortInfoList_ptr * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = alloc_custom(&camlidl_cops_gphoto2_GPPortInfoList_ptr, sizeof(GPPortInfoList_ptr), 0, 1);
  *((GPPortInfoList_ptr *) Data_custom_val(_v1)) = *_c2;
  return _v1;
}

void camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(value _v1, CameraAbilitiesList_ptr * _c2, camlidl_ctx _ctx)
{
  *_c2 = *((CameraAbilitiesList_ptr *) Data_custom_val(_v1));
}

static void camlidl_finalize_gphoto2_CameraAbilitiesList_ptr(value v)
{
  finalize_camera_abilities((CameraAbilitiesList_ptr *) Data_custom_val(v));
}
struct custom_operations camlidl_cops_gphoto2_CameraAbilitiesList_ptr = {
  NULL,
  camlidl_finalize_gphoto2_CameraAbilitiesList_ptr,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

value camlidl_c2ml_gphoto2_CameraAbilitiesList_ptr(CameraAbilitiesList_ptr * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = alloc_custom(&camlidl_cops_gphoto2_CameraAbilitiesList_ptr, sizeof(CameraAbilitiesList_ptr), 0, 1);
  *((CameraAbilitiesList_ptr *) Data_custom_val(_v1)) = *_c2;
  return _v1;
}

void camlidl_ml2c_gphoto2_CameraList_ptr(value _v1, CameraList_ptr * _c2, camlidl_ctx _ctx)
{
  *_c2 = *((CameraList_ptr *) Data_custom_val(_v1));
}

static void camlidl_finalize_gphoto2_CameraList_ptr(value v)
{
  finalize_camera_list((CameraList_ptr *) Data_custom_val(v));
}
struct custom_operations camlidl_cops_gphoto2_CameraList_ptr = {
  NULL,
  camlidl_finalize_gphoto2_CameraList_ptr,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

value camlidl_c2ml_gphoto2_CameraList_ptr(CameraList_ptr * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = alloc_custom(&camlidl_cops_gphoto2_CameraList_ptr, sizeof(CameraList_ptr), 0, 1);
  *((CameraList_ptr *) Data_custom_val(_v1)) = *_c2;
  return _v1;
}

void camlidl_ml2c_gphoto2_CameraWidget_ptr(value _v1, CameraWidget_ptr * _c2, camlidl_ctx _ctx)
{
  *_c2 = *((CameraWidget_ptr *) Data_custom_val(_v1));
}

static void camlidl_finalize_gphoto2_CameraWidget_ptr(value v)
{
  finalize_camera_widget((CameraWidget_ptr *) Data_custom_val(v));
}
struct custom_operations camlidl_cops_gphoto2_CameraWidget_ptr = {
  NULL,
  camlidl_finalize_gphoto2_CameraWidget_ptr,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

value camlidl_c2ml_gphoto2_CameraWidget_ptr(CameraWidget_ptr * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = alloc_custom(&camlidl_cops_gphoto2_CameraWidget_ptr, sizeof(CameraWidget_ptr), 0, 1);
  *((CameraWidget_ptr *) Data_custom_val(_v1)) = *_c2;
  return _v1;
}

void camlidl_ml2c_gphoto2_GPPortInfo(value _v1, GPPortInfo * _c2, camlidl_ctx _ctx)
{
  *_c2 = *((GPPortInfo *) Data_custom_val(_v1));
}

static void camlidl_finalize_gphoto2_GPPortInfo(value v)
{
  finalize_port_info((GPPortInfo *) Data_custom_val(v));
}
struct custom_operations camlidl_cops_gphoto2_GPPortInfo = {
  NULL,
  camlidl_finalize_gphoto2_GPPortInfo,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

value camlidl_c2ml_gphoto2_GPPortInfo(GPPortInfo * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = alloc_custom(&camlidl_cops_gphoto2_GPPortInfo, sizeof(GPPortInfo), 0, 1);
  *((GPPortInfo *) Data_custom_val(_v1)) = *_c2;
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_1[3] = {
  GP_CAPTURE_IMAGE,
  GP_CAPTURE_MOVIE,
  GP_CAPTURE_SOUND,
};

void camlidl_ml2c_gphoto2_CameraCaptureType(value _v1, CameraCaptureType * _c2, camlidl_ctx _ctx)
{
  (*_c2) = camlidl_transl_table_gphoto2_enum_1[Int_val(_v1)];
}

value camlidl_c2ml_gphoto2_CameraCaptureType(CameraCaptureType * _c2, camlidl_ctx _ctx)
{
value _v1;
  switch((*_c2)) {
  case GP_CAPTURE_IMAGE: _v1 = Val_int(0); break;
  case GP_CAPTURE_MOVIE: _v1 = Val_int(1); break;
  case GP_CAPTURE_SOUND: _v1 = Val_int(2); break;
  default: invalid_argument("typedef CameraCaptureType: bad enum  value");
  }
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_2[5] = {
  GP_PORT_NONE,
  GP_PORT_SERIAL,
  GP_PORT_USB,
  GP_PORT_DISK,
  GP_PORT_PTPIP,
};

int camlidl_ml2c_gphoto2_enum__GPPortType(value _v1)
{
  int _c2;
  _c2 = camlidl_transl_table_gphoto2_enum_2[Int_val(_v1)];
  return _c2;
}

value camlidl_c2ml_gphoto2_enum__GPPortType(int _c1)
{
  value _v2;
  _v2 = camlidl_find_enum(_c1, camlidl_transl_table_gphoto2_enum_2, 5, "enum _GPPortType: bad enum _GPPortType value");
  return _v2;
}

void camlidl_ml2c_gphoto2_GPPortType(value _v1, GPPortType * _c2, camlidl_ctx _ctx)
{
  (*_c2) = convert_flag_list(_v1, camlidl_transl_table_gphoto2_enum_2);
}

value camlidl_c2ml_gphoto2_GPPortType(GPPortType * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_alloc_flag_list((*_c2), camlidl_transl_table_gphoto2_enum_2, 5);
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_3[4] = {
  GP_DRIVER_STATUS_PRODUCTION,
  GP_DRIVER_STATUS_TESTING,
  GP_DRIVER_STATUS_EXPERIMENTAL,
  GP_DRIVER_STATUS_DEPRECATED,
};

void camlidl_ml2c_gphoto2_CameraDriverStatus(value _v1, CameraDriverStatus * _c2, camlidl_ctx _ctx)
{
  (*_c2) = camlidl_transl_table_gphoto2_enum_3[Int_val(_v1)];
}

value camlidl_c2ml_gphoto2_CameraDriverStatus(CameraDriverStatus * _c2, camlidl_ctx _ctx)
{
value _v1;
  switch((*_c2)) {
  case GP_DRIVER_STATUS_PRODUCTION: _v1 = Val_int(0); break;
  case GP_DRIVER_STATUS_TESTING: _v1 = Val_int(1); break;
  case GP_DRIVER_STATUS_EXPERIMENTAL: _v1 = Val_int(2); break;
  case GP_DRIVER_STATUS_DEPRECATED: _v1 = Val_int(3); break;
  default: invalid_argument("typedef CameraDriverStatus: bad enum  value");
  }
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_4[2] = {
  GP_DEVICE_STILL_CAMERA,
  GP_DEVICE_AUDIO_PLAYER,
};

void camlidl_ml2c_gphoto2_GphotoDeviceType(value _v1, GphotoDeviceType * _c2, camlidl_ctx _ctx)
{
  (*_c2) = camlidl_transl_table_gphoto2_enum_4[Int_val(_v1)];
}

value camlidl_c2ml_gphoto2_GphotoDeviceType(GphotoDeviceType * _c2, camlidl_ctx _ctx)
{
value _v1;
  switch((*_c2)) {
  case GP_DEVICE_STILL_CAMERA: _v1 = Val_int(0); break;
  case GP_DEVICE_AUDIO_PLAYER: _v1 = Val_int(1); break;
  default: invalid_argument("typedef GphotoDeviceType: bad enum  value");
  }
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_5[6] = {
  GP_OPERATION_NONE,
  GP_OPERATION_CAPTURE_IMAGE,
  GP_OPERATION_CAPTURE_VIDEO,
  GP_OPERATION_CAPTURE_AUDIO,
  GP_OPERATION_CAPTURE_PREVIEW,
  GP_OPERATION_CONFIG,
};

int camlidl_ml2c_gphoto2_enum__CameraOperation(value _v1)
{
  int _c2;
  _c2 = camlidl_transl_table_gphoto2_enum_5[Int_val(_v1)];
  return _c2;
}

value camlidl_c2ml_gphoto2_enum__CameraOperation(int _c1)
{
  value _v2;
  _v2 = camlidl_find_enum(_c1, camlidl_transl_table_gphoto2_enum_5, 6, "enum _CameraOperation: bad enum _CameraOperation value");
  return _v2;
}

void camlidl_ml2c_gphoto2_CameraOperation(value _v1, CameraOperation * _c2, camlidl_ctx _ctx)
{
  (*_c2) = convert_flag_list(_v1, camlidl_transl_table_gphoto2_enum_5);
}

value camlidl_c2ml_gphoto2_CameraOperation(CameraOperation * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_alloc_flag_list((*_c2), camlidl_transl_table_gphoto2_enum_5, 6);
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_6[6] = {
  GP_FILE_OPERATION_NONE,
  GP_FILE_OPERATION_DELETE,
  GP_FILE_OPERATION_PREVIEW,
  GP_FILE_OPERATION_RAW,
  GP_FILE_OPERATION_AUDIO,
  GP_FILE_OPERATION_EXIF,
};

int camlidl_ml2c_gphoto2_enum__CameraFileOperation(value _v1)
{
  int _c2;
  _c2 = camlidl_transl_table_gphoto2_enum_6[Int_val(_v1)];
  return _c2;
}

value camlidl_c2ml_gphoto2_enum__CameraFileOperation(int _c1)
{
  value _v2;
  _v2 = camlidl_find_enum(_c1, camlidl_transl_table_gphoto2_enum_6, 6, "enum _CameraFileOperation: bad enum _CameraFileOperation value");
  return _v2;
}

void camlidl_ml2c_gphoto2_CameraFileOperation(value _v1, CameraFileOperation * _c2, camlidl_ctx _ctx)
{
  (*_c2) = convert_flag_list(_v1, camlidl_transl_table_gphoto2_enum_6);
}

value camlidl_c2ml_gphoto2_CameraFileOperation(CameraFileOperation * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_alloc_flag_list((*_c2), camlidl_transl_table_gphoto2_enum_6, 6);
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_7[5] = {
  GP_FOLDER_OPERATION_NONE,
  GP_FOLDER_OPERATION_DELETE_ALL,
  GP_FOLDER_OPERATION_PUT_FILE,
  GP_FOLDER_OPERATION_MAKE_DIR,
  GP_FOLDER_OPERATION_REMOVE_DIR,
};

int camlidl_ml2c_gphoto2_enum__CameraFolderOperation(value _v1)
{
  int _c2;
  _c2 = camlidl_transl_table_gphoto2_enum_7[Int_val(_v1)];
  return _c2;
}

value camlidl_c2ml_gphoto2_enum__CameraFolderOperation(int _c1)
{
  value _v2;
  _v2 = camlidl_find_enum(_c1, camlidl_transl_table_gphoto2_enum_7, 5, "enum _CameraFolderOperation: bad enum _CameraFolderOperation value");
  return _v2;
}

void camlidl_ml2c_gphoto2_CameraFolderOperation(value _v1, CameraFolderOperation * _c2, camlidl_ctx _ctx)
{
  (*_c2) = convert_flag_list(_v1, camlidl_transl_table_gphoto2_enum_7);
}

value camlidl_c2ml_gphoto2_CameraFolderOperation(CameraFolderOperation * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_alloc_flag_list((*_c2), camlidl_transl_table_gphoto2_enum_7, 5);
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_8[9] = {
  GP_STORAGEINFO_BASE,
  GP_STORAGEINFO_LABEL,
  GP_STORAGEINFO_DESCRIPTION,
  GP_STORAGEINFO_ACCESS,
  GP_STORAGEINFO_STORAGETYPE,
  GP_STORAGEINFO_FILESYSTEMTYPE,
  GP_STORAGEINFO_MAXCAPACITY,
  GP_STORAGEINFO_FREESPACEKBYTES,
  GP_STORAGEINFO_FREESPACEIMAGES,
};

int camlidl_ml2c_gphoto2_enum__CameraStorageInfoFields(value _v1)
{
  int _c2;
  _c2 = camlidl_transl_table_gphoto2_enum_8[Int_val(_v1)];
  return _c2;
}

value camlidl_c2ml_gphoto2_enum__CameraStorageInfoFields(int _c1)
{
  value _v2;
  _v2 = camlidl_find_enum(_c1, camlidl_transl_table_gphoto2_enum_8, 9, "enum _CameraStorageInfoFields: bad enum _CameraStorageInfoFields value");
  return _v2;
}

void camlidl_ml2c_gphoto2_CameraStorageInfoFields(value _v1, CameraStorageInfoFields * _c2, camlidl_ctx _ctx)
{
  (*_c2) = convert_flag_list(_v1, camlidl_transl_table_gphoto2_enum_8);
}

value camlidl_c2ml_gphoto2_CameraStorageInfoFields(CameraStorageInfoFields * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_alloc_flag_list((*_c2), camlidl_transl_table_gphoto2_enum_8, 9);
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_9[5] = {
  GP_STORAGEINFO_ST_UNKNOWN,
  GP_STORAGEINFO_ST_FIXED_ROM,
  GP_STORAGEINFO_ST_REMOVABLE_ROM,
  GP_STORAGEINFO_ST_FIXED_RAM,
  GP_STORAGEINFO_ST_REMOVABLE_RAM,
};

void camlidl_ml2c_gphoto2_CameraStorageType(value _v1, CameraStorageType * _c2, camlidl_ctx _ctx)
{
  (*_c2) = camlidl_transl_table_gphoto2_enum_9[Int_val(_v1)];
}

value camlidl_c2ml_gphoto2_CameraStorageType(CameraStorageType * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_find_enum((*_c2), camlidl_transl_table_gphoto2_enum_9, 5, "typedef CameraStorageType: bad enum  value");
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_10[3] = {
  GP_STORAGEINFO_AC_READWRITE,
  GP_STORAGEINFO_AC_READONLY,
  GP_STORAGEINFO_AC_READONLY_WITH_DELETE,
};

void camlidl_ml2c_gphoto2_CameraStorageAccessType(value _v1, CameraStorageAccessType * _c2, camlidl_ctx _ctx)
{
  (*_c2) = camlidl_transl_table_gphoto2_enum_10[Int_val(_v1)];
}

value camlidl_c2ml_gphoto2_CameraStorageAccessType(CameraStorageAccessType * _c2, camlidl_ctx _ctx)
{
value _v1;
  switch((*_c2)) {
  case GP_STORAGEINFO_AC_READWRITE: _v1 = Val_int(0); break;
  case GP_STORAGEINFO_AC_READONLY: _v1 = Val_int(1); break;
  case GP_STORAGEINFO_AC_READONLY_WITH_DELETE: _v1 = Val_int(2); break;
  default: invalid_argument("typedef CameraStorageAccessType: bad enum  value");
  }
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_11[4] = {
  GP_STORAGEINFO_FST_UNDEFINED,
  GP_STORAGEINFO_FST_GENERICFLAT,
  GP_STORAGEINFO_FST_GENERICHIERARCHICAL,
  GP_STORAGEINFO_FST_DCF,
};

void camlidl_ml2c_gphoto2_CameraStorageFilesystemType(value _v1, CameraStorageFilesystemType * _c2, camlidl_ctx _ctx)
{
  (*_c2) = camlidl_transl_table_gphoto2_enum_11[Int_val(_v1)];
}

value camlidl_c2ml_gphoto2_CameraStorageFilesystemType(CameraStorageFilesystemType * _c2, camlidl_ctx _ctx)
{
value _v1;
  switch((*_c2)) {
  case GP_STORAGEINFO_FST_UNDEFINED: _v1 = Val_int(0); break;
  case GP_STORAGEINFO_FST_GENERICFLAT: _v1 = Val_int(1); break;
  case GP_STORAGEINFO_FST_GENERICHIERARCHICAL: _v1 = Val_int(2); break;
  case GP_STORAGEINFO_FST_DCF: _v1 = Val_int(3); break;
  default: invalid_argument("typedef CameraStorageFilesystemType: bad enum  value");
  }
  return _v1;
}

void camlidl_ml2c_gphoto2_struct__CameraStorageInformation(value _v1, struct _CameraStorageInformation * _c2, camlidl_ctx _ctx)
{
  value _v3;
  value _v4;
  value _v5;
  value _v6;
  value _v7;
  value _v8;
  value _v9;
  value _v10;
  value _v11;
  value _v12;
  _v3 = Field(_v1, 0);
  camlidl_ml2c_gphoto2_CameraStorageInfoFields(_v3, &(*_c2).fields, _ctx);
  _v4 = Field(_v1, 1);
  if (string_length(_v4) >= 256) invalid_argument("struct _CameraStorageInformation");
  strcpy((*_c2).basedir, String_val(_v4));
  _v5 = Field(_v1, 2);
  if (string_length(_v5) >= 256) invalid_argument("struct _CameraStorageInformation");
  strcpy((*_c2).label, String_val(_v5));
  _v6 = Field(_v1, 3);
  if (string_length(_v6) >= 256) invalid_argument("struct _CameraStorageInformation");
  strcpy((*_c2).description, String_val(_v6));
  _v7 = Field(_v1, 4);
  camlidl_ml2c_gphoto2_CameraStorageType(_v7, &(*_c2).type, _ctx);
  _v8 = Field(_v1, 5);
  camlidl_ml2c_gphoto2_CameraStorageFilesystemType(_v8, &(*_c2).fstype, _ctx);
  _v9 = Field(_v1, 6);
  camlidl_ml2c_gphoto2_CameraStorageAccessType(_v9, &(*_c2).access, _ctx);
  _v10 = Field(_v1, 7);
  (*_c2).capacitykbytes = Long_val(_v10);
  _v11 = Field(_v1, 8);
  (*_c2).freekbytes = Long_val(_v11);
  _v12 = Field(_v1, 9);
  (*_c2).freeimages = Long_val(_v12);
}

value camlidl_c2ml_gphoto2_struct__CameraStorageInformation(struct _CameraStorageInformation * _c1, camlidl_ctx _ctx)
{
  value _v2;
  value _v3[10];
  memset(_v3, 0, 10 * sizeof(value));
  Begin_roots_block(_v3, 10)
    _v3[0] = camlidl_c2ml_gphoto2_CameraStorageInfoFields(&(*_c1).fields, _ctx);
    _v3[1] = copy_string((*_c1).basedir);
    _v3[2] = copy_string((*_c1).label);
    _v3[3] = copy_string((*_c1).description);
    _v3[4] = camlidl_c2ml_gphoto2_CameraStorageType(&(*_c1).type, _ctx);
    _v3[5] = camlidl_c2ml_gphoto2_CameraStorageFilesystemType(&(*_c1).fstype, _ctx);
    _v3[6] = camlidl_c2ml_gphoto2_CameraStorageAccessType(&(*_c1).access, _ctx);
    _v3[7] = Val_long((*_c1).capacitykbytes);
    _v3[8] = Val_long((*_c1).freekbytes);
    _v3[9] = Val_long((*_c1).freeimages);
    _v2 = camlidl_alloc_small(10, 0);
    { mlsize_t _c4;
      for (_c4 = 0; _c4 < 10; _c4++) Field(_v2, _c4) = _v3[_c4];
    }
  End_roots()
  return _v2;
}

void camlidl_ml2c_gphoto2_CameraStorageInformation(value _v1, CameraStorageInformation * _c2, camlidl_ctx _ctx)
{
  camlidl_ml2c_gphoto2_struct__CameraStorageInformation(_v1, &(*_c2), _ctx);
}

value camlidl_c2ml_gphoto2_CameraStorageInformation(CameraStorageInformation * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_c2ml_gphoto2_struct__CameraStorageInformation(&(*_c2), _ctx);
  return _v1;
}

void camlidl_ml2c_gphoto2_CameraAbilities(value _v1, CameraAbilities * _c2, camlidl_ctx _ctx)
{
value _v3;
value _v4;
value _v5;
value _v6;
mlsize_t _c7;
mlsize_t _c8;
value _v9;
value _v10;
value _v11;
value _v12;
value _v13;
value _v14;
value _v15;
value _v16;
value _v17;
value _v18;
value _v19;
value _v20;
value _v21;
value _v22;
value _v23;
value _v24;
value _v25;
value _v26;
value _v27;
  _v3 = Field(_v1, 0);
  if (string_length(_v3) >= 128) invalid_argument("typedef CameraAbilities");
  strcpy((*_c2).model, String_val(_v3));
  _v4 = Field(_v1, 1);
  camlidl_ml2c_gphoto2_CameraDriverStatus(_v4, &(*_c2).status, _ctx);
  _v5 = Field(_v1, 2);
  camlidl_ml2c_gphoto2_GPPortType(_v5, &(*_c2).port, _ctx);
  _v6 = Field(_v1, 3);
  _c7 = Wosize_val(_v6);
  if (_c7 != 64) invalid_argument("typedef CameraAbilities");
  for (_c8 = 0; _c8 < 64; _c8++) {
    _v9 = Field(_v6, _c8);
    (*_c2).speed[_c8] = Int_val(_v9);
  }
  _v10 = Field(_v1, 4);
  camlidl_ml2c_gphoto2_CameraOperation(_v10, &(*_c2).operations, _ctx);
  _v11 = Field(_v1, 5);
  camlidl_ml2c_gphoto2_CameraFileOperation(_v11, &(*_c2).file_operations, _ctx);
  _v12 = Field(_v1, 6);
  camlidl_ml2c_gphoto2_CameraFolderOperation(_v12, &(*_c2).folder_operations, _ctx);
  _v13 = Field(_v1, 7);
  (*_c2).usb_vendor = Int_val(_v13);
  _v14 = Field(_v1, 8);
  (*_c2).usb_product = Int_val(_v14);
  _v15 = Field(_v1, 9);
  (*_c2).usb_class = Int_val(_v15);
  _v16 = Field(_v1, 10);
  (*_c2).usb_subclass = Int_val(_v16);
  _v17 = Field(_v1, 11);
  (*_c2).usb_protocol = Int_val(_v17);
  _v18 = Field(_v1, 12);
  if (string_length(_v18) >= 1024) invalid_argument("typedef CameraAbilities");
  strcpy((*_c2).library, String_val(_v18));
  _v19 = Field(_v1, 13);
  if (string_length(_v19) >= 1024) invalid_argument("typedef CameraAbilities");
  strcpy((*_c2).id, String_val(_v19));
  _v20 = Field(_v1, 14);
  camlidl_ml2c_gphoto2_GphotoDeviceType(_v20, &(*_c2).device_type, _ctx);
  _v21 = Field(_v1, 15);
  (*_c2).reserved2 = Int_val(_v21);
  _v22 = Field(_v1, 16);
  (*_c2).reserved3 = Int_val(_v22);
  _v23 = Field(_v1, 17);
  (*_c2).reserved4 = Int_val(_v23);
  _v24 = Field(_v1, 18);
  (*_c2).reserved5 = Int_val(_v24);
  _v25 = Field(_v1, 19);
  (*_c2).reserved6 = Int_val(_v25);
  _v26 = Field(_v1, 20);
  (*_c2).reserved7 = Int_val(_v26);
  _v27 = Field(_v1, 21);
  (*_c2).reserved8 = Int_val(_v27);
}

value camlidl_c2ml_gphoto2_CameraAbilities(CameraAbilities * _c2, camlidl_ctx _ctx)
{
value _v1;
value _v3[22];
mlsize_t _c4;
value _v5;
  memset(_v3, 0, 22 * sizeof(value));
  Begin_roots_block(_v3, 22)
    _v3[0] = copy_string((*_c2).model);
    _v3[1] = camlidl_c2ml_gphoto2_CameraDriverStatus(&(*_c2).status, _ctx);
    _v3[2] = camlidl_c2ml_gphoto2_GPPortType(&(*_c2).port, _ctx);
    _v3[3] = camlidl_alloc(64, 0);
    for (_c4 = 0; _c4 < 64; _c4++) {
      _v5 = Val_int((*_c2).speed[_c4]);
      modify(&Field(_v3[3], _c4), _v5);
    }
    _v3[4] = camlidl_c2ml_gphoto2_CameraOperation(&(*_c2).operations, _ctx);
    _v3[5] = camlidl_c2ml_gphoto2_CameraFileOperation(&(*_c2).file_operations, _ctx);
    _v3[6] = camlidl_c2ml_gphoto2_CameraFolderOperation(&(*_c2).folder_operations, _ctx);
    _v3[7] = Val_int((*_c2).usb_vendor);
    _v3[8] = Val_int((*_c2).usb_product);
    _v3[9] = Val_int((*_c2).usb_class);
    _v3[10] = Val_int((*_c2).usb_subclass);
    _v3[11] = Val_int((*_c2).usb_protocol);
    _v3[12] = copy_string((*_c2).library);
    _v3[13] = copy_string((*_c2).id);
    _v3[14] = camlidl_c2ml_gphoto2_GphotoDeviceType(&(*_c2).device_type, _ctx);
    _v3[15] = Val_int((*_c2).reserved2);
    _v3[16] = Val_int((*_c2).reserved3);
    _v3[17] = Val_int((*_c2).reserved4);
    _v3[18] = Val_int((*_c2).reserved5);
    _v3[19] = Val_int((*_c2).reserved6);
    _v3[20] = Val_int((*_c2).reserved7);
    _v3[21] = Val_int((*_c2).reserved8);
    _v1 = camlidl_alloc_small(22, 0);
    { mlsize_t _c6;
      for (_c6 = 0; _c6 < 22; _c6++) Field(_v1, _c6) = _v3[_c6];
    }
  End_roots()
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_14[6] = {
  GP_FILE_TYPE_PREVIEW,
  GP_FILE_TYPE_NORMAL,
  GP_FILE_TYPE_RAW,
  GP_FILE_TYPE_AUDIO,
  GP_FILE_TYPE_EXIF,
  GP_FILE_TYPE_METADATA,
};

void camlidl_ml2c_gphoto2_CameraFileType(value _v1, CameraFileType * _c2, camlidl_ctx _ctx)
{
  (*_c2) = camlidl_transl_table_gphoto2_enum_14[Int_val(_v1)];
}

value camlidl_c2ml_gphoto2_CameraFileType(CameraFileType * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_find_enum((*_c2), camlidl_transl_table_gphoto2_enum_14, 6, "typedef CameraFileType: bad enum  value");
  return _v1;
}

int camlidl_transl_table_gphoto2_enum_15[9] = {
  GP_WIDGET_WINDOW,
  GP_WIDGET_SECTION,
  GP_WIDGET_TEXT,
  GP_WIDGET_RANGE,
  GP_WIDGET_TOGGLE,
  GP_WIDGET_RADIO,
  GP_WIDGET_MENU,
  GP_WIDGET_BUTTON,
  GP_WIDGET_DATE,
};

void camlidl_ml2c_gphoto2_CameraWidgetType(value _v1, CameraWidgetType * _c2, camlidl_ctx _ctx)
{
  (*_c2) = camlidl_transl_table_gphoto2_enum_15[Int_val(_v1)];
}

value camlidl_c2ml_gphoto2_CameraWidgetType(CameraWidgetType * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = camlidl_find_enum((*_c2), camlidl_transl_table_gphoto2_enum_15, 9, "typedef CameraWidgetType: bad enum  value");
  return _v1;
}

void camlidl_ml2c_gphoto2_fdescr(value _v1, fdescr * _c2, camlidl_ctx _ctx)
{
  (*_c2) = Int_val(_v1);
}

value camlidl_c2ml_gphoto2_fdescr(fdescr * _c2, camlidl_ctx _ctx)
{
value _v1;
  _v1 = Val_int((*_c2));
  return _v1;
}

value camlidl_gphoto2_gp_context_new(value _unit)
{
  GPContext_ptr _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  _res = gp_context_new();
  _vres = camlidl_c2ml_gphoto2_GPContext_ptr(&_res, _ctx);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_debug_init(value _unit)
{
  debug_init();
  return Val_unit;
}

value camlidl_gphoto2_gp_port_info_list_new(value _unit)
{
  GPPortInfoList_ptr *list; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  GPPortInfoList_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  list = &_c1;
  _res = gp_port_info_list_new(list);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_GPPortInfoList_ptr(&*list, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_port_info_list_free(
	value _v_list)
{
  GPPortInfoList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfoList_ptr(_v_list, &list, _ctx);
  _res = gp_port_info_list_free(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_port_info_list_load(
	value _v_list)
{
  GPPortInfoList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfoList_ptr(_v_list, &list, _ctx);
  _res = gp_port_info_list_load(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_port_info_list_count(
	value _v_list)
{
  GPPortInfoList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfoList_ptr(_v_list, &list, _ctx);
  _res = gp_port_info_list_count(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_port_info_list_get_info(
	value _v_list,
	value _v_n)
{
  GPPortInfoList_ptr list; /*in*/
  int n; /*in*/
  GPPortInfo *info; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  GPPortInfo _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_GPPortInfoList_ptr(_v_list, &list, _ctx);
  n = Int_val(_v_n);
  info = &_c1;
  _res = gp_port_info_list_get_info(list, n, info);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_GPPortInfo(&*info, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_port_info_list_lookup_path(
	value _v_list,
	value _v_path)
{
  GPPortInfoList_ptr list; /*in*/
  char const *path; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfoList_ptr(_v_list, &list, _ctx);
  path = String_val(_v_path);
  _res = gp_port_info_list_lookup_path(list, path);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_port_info_list_lookup_name(
	value _v_list,
	value _v_name)
{
  GPPortInfoList_ptr list; /*in*/
  char const *name; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfoList_ptr(_v_list, &list, _ctx);
  name = String_val(_v_name);
  _res = gp_port_info_list_lookup_name(list, name);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_port_info_new(value _unit)
{
  GPPortInfo *info; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  GPPortInfo _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  info = &_c1;
  _res = gp_port_info_new(info);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_GPPortInfo(&*info, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_port_info_get_name(
	value _v_info)
{
  GPPortInfo info; /*in*/
  char **name; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_GPPortInfo(_v_info, &info, _ctx);
  name = &_c1;
  _res = gp_port_info_get_name(info, name);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*name);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_port_info_set_name(
	value _v_info,
	value _v_name)
{
  GPPortInfo info; /*in*/
  char const *name; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfo(_v_info, &info, _ctx);
  name = String_val(_v_name);
  _res = gp_port_info_set_name(info, name);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_port_info_get_path(
	value _v_info)
{
  GPPortInfo info; /*in*/
  char **path; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_GPPortInfo(_v_info, &info, _ctx);
  path = &_c1;
  _res = gp_port_info_get_path(info, path);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*path);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_port_info_set_path(
	value _v_info,
	value _v_path)
{
  GPPortInfo info; /*in*/
  char const *path; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfo(_v_info, &info, _ctx);
  path = String_val(_v_path);
  _res = gp_port_info_set_path(info, path);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_port_info_get_type(
	value _v_info,
	value _v_type)
{
  GPPortInfo info; /*in*/
  GPPortType *type; /*in*/
  int _res;
  value _v1;
  GPPortType _c2;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfo(_v_info, &info, _ctx);
  if (_v_type == Val_int(0)) {
    type = NULL;
  } else {
    _v1 = Field(_v_type, 0);
    type = &_c2;
    camlidl_ml2c_gphoto2_GPPortType(_v1, &_c2, _ctx);
  }
  _res = gp_port_info_get_type(info, type);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_port_info_set_type(
	value _v_info,
	value _v_type)
{
  GPPortInfo info; /*in*/
  GPPortType type; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_GPPortInfo(_v_info, &info, _ctx);
  camlidl_ml2c_gphoto2_GPPortType(_v_type, &type, _ctx);
  _res = gp_port_info_set_type(info, type);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_file_new(value _unit)
{
  CameraFile_ptr *file; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraFile_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  file = &_c1;
  _res = gp_file_new(file);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraFile_ptr(&*file, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_file_new_from_fd(
	value _v_fd)
{
  CameraFile_ptr *file; /*out*/
  fdescr fd; /*in*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraFile_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_fdescr(_v_fd, &fd, _ctx);
  file = &_c1;
  _res = gp_file_new_from_fd(file, fd);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraFile_ptr(&*file, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_file_free(
	value _v_file)
{
  CameraFile_ptr file; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraFile_ptr(_v_file, &file, _ctx);
  _res = gp_file_free(file);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_file_open(
	value _v_file,
	value _v_filename)
{
  CameraFile_ptr file; /*in*/
  char const *filename; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraFile_ptr(_v_file, &file, _ctx);
  filename = String_val(_v_filename);
  _res = gp_file_open(file, filename);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_file_delete(
	value _v_camera,
	value _v_folder,
	value _v_file,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  char const *folder; /*in*/
  char const *file; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  folder = String_val(_v_folder);
  file = String_val(_v_file);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_file_delete(camera, folder, file, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_folder_list_files(
	value _v_camera,
	value _v_folder,
	value _v_list,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  char const *folder; /*in*/
  CameraList_ptr list; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  folder = String_val(_v_folder);
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_folder_list_files(camera, folder, list, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_folder_list_folders(
	value _v_camera,
	value _v_folder,
	value _v_list,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  char const *folder; /*in*/
  CameraList_ptr list; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  folder = String_val(_v_folder);
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_folder_list_folders(camera, folder, list, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_folder_delete_all(
	value _v_camera,
	value _v_folder,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  char const *folder; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  folder = String_val(_v_folder);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_folder_delete_all(camera, folder, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_folder_put_file(
	value _v_camera,
	value _v_folder,
	value _v_filename,
	value _v_type,
	value _v_file,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  char const *folder; /*in*/
  char const *filename; /*in*/
  CameraFileType type; /*in*/
  CameraFile_ptr file; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  folder = String_val(_v_folder);
  filename = String_val(_v_filename);
  camlidl_ml2c_gphoto2_CameraFileType(_v_type, &type, _ctx);
  camlidl_ml2c_gphoto2_CameraFile_ptr(_v_file, &file, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_folder_put_file(camera, folder, filename, type, file, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_folder_put_file_bytecode(value * argv, int argn)
{
  return camlidl_gphoto2_gp_camera_folder_put_file(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
}

value camlidl_gphoto2_gp_camera_folder_make_dir(
	value _v_camera,
	value _v_folder,
	value _v_name,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  char const *folder; /*in*/
  char const *name; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  folder = String_val(_v_folder);
  name = String_val(_v_name);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_folder_make_dir(camera, folder, name, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_folder_remove_dir(
	value _v_camera,
	value _v_folder,
	value _v_name,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  char const *folder; /*in*/
  char const *name; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  folder = String_val(_v_folder);
  name = String_val(_v_name);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_folder_remove_dir(camera, folder, name, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_abilities_list_new(value _unit)
{
  CameraAbilitiesList_ptr *list; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraAbilitiesList_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  list = &_c1;
  _res = gp_abilities_list_new(list);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraAbilitiesList_ptr(&*list, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_abilities_list_free(
	value _v_list)
{
  CameraAbilitiesList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(_v_list, &list, _ctx);
  _res = gp_abilities_list_free(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_abilities_list_load(
	value _v_list,
	value _v_context)
{
  CameraAbilitiesList_ptr list; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(_v_list, &list, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_abilities_list_load(list, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_abilities_list_reset(
	value _v_list)
{
  CameraAbilitiesList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(_v_list, &list, _ctx);
  _res = gp_abilities_list_reset(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_abilities_list_detect(
	value _v_list,
	value _v_info_list,
	value _v_l,
	value _v_context)
{
  CameraAbilitiesList_ptr list; /*in*/
  GPPortInfoList_ptr info_list; /*in*/
  CameraList_ptr l; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(_v_list, &list, _ctx);
  camlidl_ml2c_gphoto2_GPPortInfoList_ptr(_v_info_list, &info_list, _ctx);
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_l, &l, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_abilities_list_detect(list, info_list, l, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_abilities_list_append(
	value _v_list,
	value _v_abilities)
{
  CameraAbilitiesList_ptr list; /*in*/
  CameraAbilities abilities; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(_v_list, &list, _ctx);
  camlidl_ml2c_gphoto2_CameraAbilities(_v_abilities, &abilities, _ctx);
  _res = gp_abilities_list_append(list, abilities);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_abilities_list_count(
	value _v_list)
{
  CameraAbilitiesList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(_v_list, &list, _ctx);
  _res = gp_abilities_list_count(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_abilities_list_lookup_model(
	value _v_list,
	value _v_model)
{
  CameraAbilitiesList_ptr list; /*in*/
  char const *model; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(_v_list, &list, _ctx);
  model = String_val(_v_model);
  _res = gp_abilities_list_lookup_model(list, model);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_abilities_list_get_abilities(
	value _v_list,
	value _v_index)
{
  CameraAbilitiesList_ptr list; /*in*/
  int index; /*in*/
  CameraAbilities *abilities; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraAbilities _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraAbilitiesList_ptr(_v_list, &list, _ctx);
  index = Int_val(_v_index);
  abilities = &_c1;
  _res = gp_abilities_list_get_abilities(list, index, abilities);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraAbilities(&*abilities, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_list_new(value _unit)
{
  CameraList_ptr *list; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraList_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  list = &_c1;
  _res = gp_list_new(list);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraList_ptr(&*list, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_list_ref(
	value _v_list)
{
  CameraList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  _res = gp_list_ref(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_unref(
	value _v_list)
{
  CameraList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  _res = gp_list_unref(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_free(
	value _v_list)
{
  CameraList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  _res = gp_list_free(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_count(
	value _v_list)
{
  CameraList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  _res = gp_list_count(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_append(
	value _v_list,
	value _v_name,
	value _v_v)
{
  CameraList_ptr list; /*in*/
  char const *name; /*in*/
  char const *v; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  name = String_val(_v_name);
  v = String_val(_v_v);
  _res = gp_list_append(list, name, v);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_reset(
	value _v_list)
{
  CameraList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  _res = gp_list_reset(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_sort(
	value _v_list)
{
  CameraList_ptr list; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  _res = gp_list_sort(list);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_find_by_name(
	value _v_list,
	value _v_name)
{
  CameraList_ptr list; /*in*/
  int *index; /*out*/
  char const *name; /*in*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  int _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  name = String_val(_v_name);
  index = &_c1;
  _res = gp_list_find_by_name(list, index, name);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = Val_int(*index);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_list_get_name(
	value _v_list,
	value _v_index)
{
  CameraList_ptr list; /*in*/
  int index; /*in*/
  char const **name; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char const *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  index = Int_val(_v_index);
  name = &_c1;
  _res = gp_list_get_name(list, index, name);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*name);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_list_get_value(
	value _v_list,
	value _v_index)
{
  CameraList_ptr list; /*in*/
  int index; /*in*/
  char const **v; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char const *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  index = Int_val(_v_index);
  v = &_c1;
  _res = gp_list_get_value(list, index, v);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*v);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_list_set_name(
	value _v_list,
	value _v_index,
	value _v_name)
{
  CameraList_ptr list; /*in*/
  int index; /*in*/
  char const *name; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  index = Int_val(_v_index);
  name = String_val(_v_name);
  _res = gp_list_set_name(list, index, name);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_set_value(
	value _v_list,
	value _v_index,
	value _v_v)
{
  CameraList_ptr list; /*in*/
  int index; /*in*/
  char const *v; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  index = Int_val(_v_index);
  v = String_val(_v_v);
  _res = gp_list_set_value(list, index, v);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_list_populate(
	value _v_list,
	value _v_format,
	value _v_count)
{
  CameraList_ptr list; /*in*/
  char const *format; /*in*/
  int count; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraList_ptr(_v_list, &list, _ctx);
  format = String_val(_v_format);
  count = Int_val(_v_count);
  _res = gp_list_populate(list, format, count);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_new(
	value _v_type,
	value _v_label)
{
  CameraWidgetType type; /*in*/
  char const *label; /*in*/
  CameraWidget_ptr *widget; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  value _v1;
  char _c2;
  CameraWidget_ptr _c3;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidgetType(_v_type, &type, _ctx);
  if (_v_label == Val_int(0)) {
    label = NULL;
  } else {
    _v1 = Field(_v_label, 0);
    label = &_c2;
    _c2 = Int_val(_v1);
  }
  widget = &_c3;
  _res = gp_widget_new(type, label, widget);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraWidget_ptr(&*widget, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_append(
	value _v_widget,
	value _v_child)
{
  CameraWidget_ptr widget; /*in*/
  CameraWidget_ptr child; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_child, &child, _ctx);
  _res = gp_widget_append(widget, child);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_prepend(
	value _v_widget,
	value _v_child)
{
  CameraWidget_ptr widget; /*in*/
  CameraWidget_ptr child; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_child, &child, _ctx);
  _res = gp_widget_prepend(widget, child);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_count_children(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  _res = gp_widget_count_children(widget);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_get_child(
	value _v_widget,
	value _v_child_number)
{
  CameraWidget_ptr widget; /*in*/
  int child_number; /*in*/
  CameraWidget_ptr *child; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraWidget_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  child_number = Int_val(_v_child_number);
  child = &_c1;
  _res = gp_widget_get_child(widget, child_number, child);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraWidget_ptr(&*child, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_child_by_label(
	value _v_widget,
	value _v_label)
{
  CameraWidget_ptr widget; /*in*/
  char const *label; /*in*/
  CameraWidget_ptr *child; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraWidget_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  label = String_val(_v_label);
  child = &_c1;
  _res = gp_widget_get_child_by_label(widget, label, child);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraWidget_ptr(&*child, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_child_by_id(
	value _v_widget,
	value _v_id)
{
  CameraWidget_ptr widget; /*in*/
  int id; /*in*/
  CameraWidget_ptr *child; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraWidget_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  id = Int_val(_v_id);
  child = &_c1;
  _res = gp_widget_get_child_by_id(widget, id, child);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraWidget_ptr(&*child, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_child_by_name(
	value _v_widget,
	value _v_name,
	value _v_child)
{
  CameraWidget_ptr widget; /*in*/
  char const *name; /*in*/
  CameraWidget_ptr *child; /*in*/
  int _res;
  value _v1;
  CameraWidget_ptr _c2;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  name = String_val(_v_name);
  if (_v_child == Val_int(0)) {
    child = NULL;
  } else {
    _v1 = Field(_v_child, 0);
    child = &_c2;
    camlidl_ml2c_gphoto2_CameraWidget_ptr(_v1, &_c2, _ctx);
  }
  _res = gp_widget_get_child_by_name(widget, name, child);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_get_root(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  CameraWidget_ptr *root; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraWidget_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  root = &_c1;
  _res = gp_widget_get_root(widget, root);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraWidget_ptr(&*root, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_parent(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  CameraWidget_ptr *parent; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraWidget_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  parent = &_c1;
  _res = gp_widget_get_parent(widget, parent);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraWidget_ptr(&*parent, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_set_value_int(
	value _v_widget,
	value _v_v)
{
  CameraWidget_ptr widget; /*in*/
  int v; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  v = Int_val(_v_v);
  _res = gp_widget_set_value_int(widget, v);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_set_value_float(
	value _v_widget,
	value _v_v)
{
  CameraWidget_ptr widget; /*in*/
  float v; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  v = Double_val(_v_v);
  _res = gp_widget_set_value_float(widget, v);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_set_value_string(
	value _v_widget,
	value _v_v)
{
  CameraWidget_ptr widget; /*in*/
  char *v; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  v = String_val(_v_v);
  _res = gp_widget_set_value_string(widget, v);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_get_value_int(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  int *v; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  int _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  v = &_c1;
  _res = gp_widget_get_value_int(widget, v);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = Val_int(*v);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_value_float(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  float *v; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  float _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  v = &_c1;
  _res = gp_widget_get_value_float(widget, v);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_double(*v);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_value_string(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  char **v; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  v = &_c1;
  _res = gp_widget_get_value_string(widget, v);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*v);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_set_name(
	value _v_widget,
	value _v_name)
{
  CameraWidget_ptr widget; /*in*/
  char const *name; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  name = String_val(_v_name);
  _res = gp_widget_set_name(widget, name);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_get_name(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  char const **name; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char const *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  name = &_c1;
  _res = gp_widget_get_name(widget, name);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*name);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_set_info(
	value _v_widget,
	value _v_info)
{
  CameraWidget_ptr widget; /*in*/
  char const *info; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  info = String_val(_v_info);
  _res = gp_widget_set_info(widget, info);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_get_info(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  char const **info; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char const *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  info = &_c1;
  _res = gp_widget_get_info(widget, info);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*info);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_id(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  int *id; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  int _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  id = &_c1;
  _res = gp_widget_get_id(widget, id);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = Val_int(*id);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_type(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  CameraWidgetType *type; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraWidgetType _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  type = &_c1;
  _res = gp_widget_get_type(widget, type);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraWidgetType(&*type, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_get_label(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  char const **label; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char const *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  label = &_c1;
  _res = gp_widget_get_label(widget, label);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*label);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_set_range(
	value _v_range,
	value _v_low,
	value _v_high,
	value _v_increment)
{
  CameraWidget_ptr range; /*in*/
  float low; /*in*/
  float high; /*in*/
  float increment; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_range, &range, _ctx);
  low = Double_val(_v_low);
  high = Double_val(_v_high);
  increment = Double_val(_v_increment);
  _res = gp_widget_set_range(range, low, high, increment);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_get_range(
	value _v_range)
{
  CameraWidget_ptr range; /*in*/
  float *min; /*out*/
  float *max; /*out*/
  float *increment; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  float _c1;
  float _c2;
  float _c3;
  value _vresult;
  value _vres[4] = { 0, 0, 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_range, &range, _ctx);
  min = &_c1;
  max = &_c2;
  increment = &_c3;
  _res = gp_widget_get_range(range, min, max, increment);
  Begin_roots_block(_vres, 4)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_double(*min);
    _vres[2] = copy_double(*max);
    _vres[3] = copy_double(*increment);
    _vresult = camlidl_alloc_small(4, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
    Field(_vresult, 2) = _vres[2];
    Field(_vresult, 3) = _vres[3];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_add_choice(
	value _v_widget,
	value _v_choice)
{
  CameraWidget_ptr widget; /*in*/
  char const *choice; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  choice = String_val(_v_choice);
  _res = gp_widget_add_choice(widget, choice);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_count_choices(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  _res = gp_widget_count_choices(widget);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_get_choice(
	value _v_widget,
	value _v_choice_number)
{
  CameraWidget_ptr widget; /*in*/
  int choice_number; /*in*/
  char const **choice; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  char const *_c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  choice_number = Int_val(_v_choice_number);
  choice = &_c1;
  _res = gp_widget_get_choice(widget, choice_number, choice);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = copy_string(*choice);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_widget_changed(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  _res = gp_widget_changed(widget);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_set_changed(
	value _v_widget,
	value _v_changed)
{
  CameraWidget_ptr widget; /*in*/
  int changed; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  changed = Int_val(_v_changed);
  _res = gp_widget_set_changed(widget, changed);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_set_readonly(
	value _v_widget,
	value _v_readonly)
{
  CameraWidget_ptr widget; /*in*/
  int readonly; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  readonly = Int_val(_v_readonly);
  _res = gp_widget_set_readonly(widget, readonly);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_widget_get_readonly(
	value _v_widget)
{
  CameraWidget_ptr widget; /*in*/
  int *readonly; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  int _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_widget, &widget, _ctx);
  readonly = &_c1;
  _res = gp_widget_get_readonly(widget, readonly);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = Val_int(*readonly);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_camera_new(value _unit)
{
  Camera_ptr *camera; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  Camera_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camera = &_c1;
  _res = gp_camera_new(camera);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_Camera_ptr(&*camera, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_camera_init(
	value _v_camera,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_init(camera, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_exit(
	value _v_camera,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_exit(camera, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_free(
	value _v_camera)
{
  Camera_ptr camera; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  _res = gp_camera_free(camera);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

void camlidl_ml2c_gphoto2_CameraFilePath(value _v1, CameraFilePath * _c2, camlidl_ctx _ctx)
{
value _v3;
value _v4;
  _v3 = Field(_v1, 0);
  if (string_length(_v3) >= 128) invalid_argument("typedef CameraFilePath");
  strcpy((*_c2).name, String_val(_v3));
  _v4 = Field(_v1, 1);
  if (string_length(_v4) >= 1024) invalid_argument("typedef CameraFilePath");
  strcpy((*_c2).folder, String_val(_v4));
}

value camlidl_c2ml_gphoto2_CameraFilePath(CameraFilePath * _c2, camlidl_ctx _ctx)
{
value _v1;
value _v3[2];
  _v3[0] = _v3[1] = 0;
  Begin_roots_block(_v3, 2)
    _v3[0] = copy_string((*_c2).name);
    _v3[1] = copy_string((*_c2).folder);
    _v1 = camlidl_alloc_small(2, 0);
    Field(_v1, 0) = _v3[0];
    Field(_v1, 1) = _v3[1];
  End_roots()
  return _v1;
}

value camlidl_gphoto2_gp_camera_capture(
	value _v_camera,
	value _v_type,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  CameraCaptureType type; /*in*/
  CameraFilePath *path; /*out*/
  GPContext_ptr context; /*in*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraFilePath _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_CameraCaptureType(_v_type, &type, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  path = &_c1;
  _res = gp_camera_capture(camera, type, path, context);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraFilePath(&*path, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_camera_capture_preview(
	value _v_camera,
	value _v_file,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  CameraFile_ptr file; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_CameraFile_ptr(_v_file, &file, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_capture_preview(camera, file, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_file_get(
	value _v_camera,
	value _v_folder,
	value _v_file,
	value _v_type,
	value _v_camera_file,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  char const *folder; /*in*/
  char const *file; /*in*/
  CameraFileType type; /*in*/
  CameraFile_ptr camera_file; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  folder = String_val(_v_folder);
  file = String_val(_v_file);
  camlidl_ml2c_gphoto2_CameraFileType(_v_type, &type, _ctx);
  camlidl_ml2c_gphoto2_CameraFile_ptr(_v_camera_file, &camera_file, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_file_get(camera, folder, file, type, camera_file, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_file_get_bytecode(value * argv, int argn)
{
  return camlidl_gphoto2_gp_camera_file_get(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
}

value camlidl_gphoto2_gp_camera_get_abilities(
	value _v_camera)
{
  Camera_ptr camera; /*in*/
  CameraAbilities *abilities; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraAbilities _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  abilities = &_c1;
  _res = gp_camera_get_abilities(camera, abilities);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraAbilities(&*abilities, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_camera_set_abilities(
	value _v_camera,
	value _v_abilities)
{
  Camera_ptr camera; /*in*/
  CameraAbilities abilities; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_CameraAbilities(_v_abilities, &abilities, _ctx);
  _res = gp_camera_set_abilities(camera, abilities);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_set_port_info(
	value _v_camera,
	value _v_info)
{
  Camera_ptr camera; /*in*/
  GPPortInfo info; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_GPPortInfo(_v_info, &info, _ctx);
  _res = gp_camera_set_port_info(camera, info);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

value camlidl_gphoto2_gp_camera_get_port_info(
	value _v_camera)
{
  Camera_ptr camera; /*in*/
  GPPortInfo *info; /*out*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  GPPortInfo _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  info = &_c1;
  _res = gp_camera_get_port_info(camera, info);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_GPPortInfo(&*info, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_camera_get_config(
	value _v_camera,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  CameraWidget_ptr *window; /*out*/
  GPContext_ptr context; /*in*/
  int _res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  CameraWidget_ptr _c1;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  window = &_c1;
  _res = gp_camera_get_config(camera, window, context);
  Begin_roots_block(_vres, 2)
    _vres[0] = Val_int(_res);
    _vres[1] = camlidl_c2ml_gphoto2_CameraWidget_ptr(&*window, _ctx);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

value camlidl_gphoto2_gp_camera_set_config(
	value _v_camera,
	value _v_window,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  CameraWidget_ptr window; /*in*/
  GPContext_ptr context; /*in*/
  int _res;
  value _vres;

  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_CameraWidget_ptr(_v_window, &window, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  _res = gp_camera_set_config(camera, window, context);
  _vres = Val_int(_res);
  camlidl_free(_ctx);
  return _vres;
}

CameraStorageInformation * gp_camera_get_storeinfo(Camera_ptr camera, int *n, GPContext_ptr context) { CameraStorageInformation *infos; gp_camera_get_storageinfo (camera, &infos, n, context); return infos; }
value camlidl_gphoto2_gp_camera_get_storeinfo(
	value _v_camera,
	value _v_context)
{
  Camera_ptr camera; /*in*/
  int *n; /*out*/
  GPContext_ptr context; /*in*/
  CameraStorageInformation *_res;
  struct camlidl_ctx_struct _ctxs = { CAMLIDL_TRANSIENT, NULL };
  camlidl_ctx _ctx = &_ctxs;
  int _c1;
  mlsize_t _c2;
  value _v3;
  value _vresult;
  value _vres[2] = { 0, 0, };

  camlidl_ml2c_gphoto2_Camera_ptr(_v_camera, &camera, _ctx);
  camlidl_ml2c_gphoto2_GPContext_ptr(_v_context, &context, _ctx);
  n = &_c1;
  _res = gp_camera_get_storeinfo(camera, n, context);
  Begin_roots_block(_vres, 2)
    _vres[0] = camlidl_alloc(*n, 0);
    Begin_root(_vres[0])
      for (_c2 = 0; _c2 < *n; _c2++) {
        _v3 = camlidl_c2ml_gphoto2_CameraStorageInformation(&_res[_c2], _ctx);
        modify(&Field(_vres[0], _c2), _v3);
      }
    End_roots()
    _vres[1] = Val_int(*n);
    _vresult = camlidl_alloc_small(2, 0);
    Field(_vresult, 0) = _vres[0];
    Field(_vresult, 1) = _vres[1];
  End_roots()
  camlidl_free(_ctx);
  return _vresult;
}

